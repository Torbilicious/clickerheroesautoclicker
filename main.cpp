#include <windows.h>
#include <iostream>
#include <winable.h>

using namespace std;

bool KeyIsPressed(unsigned char k) {
    USHORT status = (USHORT) GetAsyncKeyState(k);
    return (((status & 0x8000) >> 15) == 1) || ((status & 1) == 1);
}

void leftClick() {
    INPUT Input = {0};

    // left down
    Input.type = INPUT_MOUSE;
    Input.mi.dwFlags = MOUSEEVENTF_LEFTDOWN;
    ::SendInput(1, &Input, sizeof(INPUT));

    // left up
    ::ZeroMemory(&Input, sizeof(INPUT));
    Input.type = INPUT_MOUSE;
    Input.mi.dwFlags = MOUSEEVENTF_LEFTUP;
    ::SendInput(1, &Input, sizeof(INPUT));
}

int WINAPI WinMain(HINSTANCE hInst, HINSTANCE P, LPSTR CMD, int nShowCmd) {

    HWND target = GetForegroundWindow();
    //	pt is for the mouse position, wrect is for the client information. The mouse coords to be sent to the window
    //		must be relative to the top-left corner of the client area, so I have to include that too.
    POINT pt;
    RECT wrect;

    cout << "Started" << '\n';

    while (true) {

        //	ctrl + T to make the front-most window the targeted window
        if (KeyIsPressed(VK_CONTROL) && KeyIsPressed('T')) {
            target = GetForegroundWindow();
            char text[256];
            GetWindowText(target, text, sizeof(text));

            cout << "Targeting window:\n" << '\t' << text << '\n';

            Sleep(100);
        }

        if (KeyIsPressed(VK_ESCAPE)) {
            break;
        }

        if (KeyIsPressed('C')) {
            leftClick();

            //GetCursorPos(&pt);
            //GetClientRect(target, &wrect);
            //cout << "Clicked at:" << pt.x - wrect.left << " | " << pt.y - wrect.top << '\n';

            Sleep(5);
        }
    }

    return EXIT_SUCCESS;
}
